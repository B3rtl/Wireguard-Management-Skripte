#!/bin/bash
VPN_HOST=vpn.flinkebits.de

if [ $# -eq 0 ]
then
        echo "must pass a client name as an arg: mk-client.sh new-client"
else
        umask 077
        echo "Creating client config for: $1"
        mkdir -p clients/$1
        wg genkey | tee clients/$1/$1.priv | wg pubkey > clients/$1/$1.pub
        CLIENT_KEY=$(cat clients/$1/$1.priv)
        CLIENT_PUB_KEY=$(cat clients/$1/$1.pub)
        infix=$(cat _/last-ip.txt | tr "." " " | awk '{print $4}')
        ips="10.254.0."$(expr $infix + 1)
        ipc="10.254.0."$(expr $infix + 2)
        lastport=$(cat _/last-port.txt)
        port=$(expr $lastport + 1)
        wg genpsk > clients/$1/$1.psk
        PSK=$(cat clients/$1/$1.psk)

        wg genkey | tee clients/$1/server.priv | wg pubkey > clients/$1/server.pub
        SERVER_KEY=$(cat clients/$1/server.priv)
        SERVER_PUB_KEY=$(cat clients/$1/server.pub)


  cat _/server.conf | sed -e 's|:PSK:|'"$PSK"'|' | sed -e 's/:SERVER_IP:/'"$ips"'/' | sed -e 's|:SERVER_KEY:|'"$SERVER_KEY"'|' | sed -e 's|:CLIENT_PUB_KEY:|'"$CLIENT_PUB_KEY"'|' | sed -e 's|:PORT:|'"$port"'|' | sed -e 's|:IF_NAME:|'"wg-$1"'|' > wg-$1.conf

  cat _/client.conf | sed -e 's|:PSK:|'"$PSK"'|' | sed -e 's/:CLIENT_IP:/'"$ipc"'/' | sed -e 's|:CLIENT_KEY:|'"$CLIENT_KEY"'|' | sed -e 's|:SERVER_PUB_KEY:|'"$SERVER_PUB_KEY"'|' | sed -e 's|:PORT:|'"$port"'|' | sed -e 's|:SERVER_ADDRESS:|'"$VPN_HOST"'|' > clients/$1/$1.conf

        echo "Erzeuge in clients/$1 $1.priv, $1.pub, server.priv, server.pub"
        echo "Erzeuge clients/$1/$1.conf"
        echo "Erzeuge wg-$1.conf"
        echo "Speichere zuletzt verwendete IP, Port: $ipc : $port"
        echo $ipc > _/last-ip.txt
        echo $port > _/last-port.txt
        #cp SETUP.txt clients/$1/SETUP.txt
        #tar czvf clients/$1.tar.gz clients/$1
        echo "Konfig fertig!"
        #echo "Adding peer to hosts file"
        #echo $ipc" "$1 | sudo tee -a /etc/hosts
        #sudo wg show
        #qrencode -t ansiutf8 < clients/$1/wg0.conf

        read -p "Aktivieren von $1 in systemctl? (y/n) " yn

        case $yn in 
                [yY] ) echo ok, we will proceed;
                        systemctl enable wg-quick@wg-$1.service
                        systemctl start wg-quick@wg-$1
                        ;;
                * ) echo exiting...;
                exit;;
        esac
fi
nted.
