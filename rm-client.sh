#!/bin/bash

if [ $# -eq 0 ]
then
        echo "must pass a client name as an arg: $0 aclient"
else
        rm -rfv "/etc/wireguard/clients/$1/"
        rm -v "/etc/wireguard/wg-$1.conf"
        systemctl stop wg-quick@wg-$1
        wg-quick down wg-$1
        systemctl enable wg-quick@wg-$1.service

fi
