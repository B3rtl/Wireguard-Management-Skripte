# Wireguard-Management-Skripte

wireguard user-config skripte - managen mehrere client configs automatisch

In diesem Repo befinden sich einige Skripte, die es mit einem Kommando erlauben, einen neuen Wireguard-Nutzer zu erstellen. Dabei entsteht jeweils ein neues Öffentlich-Privates Schlüsselpaar für Server und Client. Weiterhin werden die IP-Adressen und der Port automatisch hochgezählt.
-------
This repo contains some scripts to easily manage wireguard configs for server and client. For each new config, a public-private key pair is created for server and client. Also ip addresses and port is automatically cou
